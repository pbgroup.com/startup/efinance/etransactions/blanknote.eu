<?php

namespace pbgroupeu\gettingnote_eu\Controller\User;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Laminas\Diactoros\Response;
use pbgroupeu\gettingnote_eu\Entity\Note;
use pbgroupeu\stacer_eu\Entity\User;

use pbgroupeu\stacer_eu\Controller\User\ETLBasicsController as BaseETLController;

class ETLBasicsController extends BaseETLController
{
  /**
     * Total notes
     *
     * @var ServerRequestInterface $request
     *
     * @return ResponseInterface
   */
  public function getTotalNotes(ServerRequestInterface $request): ResponseInterface
  {
    $emn = $this->getEmn();
    $user = $emn->find(User::class, $request->getQueryParams()['user_id']);

    if (!$user) {
      throw new \Exception('User not found');
    }
    
    $totalNotes = $emn->getRepository(Note::class)->getTotalUserNotes($user);
    $response = new Response();
    $response->getBody()->write(json_encode($totalNotes));

    return $response;
  }

}
