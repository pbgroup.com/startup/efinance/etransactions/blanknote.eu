<?php

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use pbgroupeu\stacer_eu\Controller\Administration\Frontend\DashboardBasics;
use pbgroupeu\stacer_eu\Controller\User\Frontend\DashboardBasics as UserDashboardBasics;

$router->map('GET', '/index.php', function (ServerRequestInterface $request) use ($container, $eniac): ResponseInterface {

    $twig = $container->get('twig')[0];

    $template = $twig->load('@user/index.html.twig');

    $response = new Laminas\Diactoros\Response;
    $response->getBody()->write($template->render([
      'target' => 'User',
    ]));
    return $response;
});

$router->map('GET', '/index.php/admin', function (ServerRequestInterface $request) use ($container, $eniac): ResponseInterface {

    $twig = $container->get('twig')[0];

    $template = $twig->load('@admin/index.html.twig');

    $response = new Laminas\Diactoros\Response;
    $response->getBody()->write($template->render([
      'target' => 'Administrator',
    ]));
    return $response;
});

// Administration scope
$router->group('/index.php/admin', function (\League\Route\RouteGroup $route) use ($container) {
    $route->map('GET', '/dashboard/basics-ETL', [$container->get(DashboardBasics::class), 'basicEtlDashboard']);
});

// User scope
$router->group('/index.php/user', function (\League\Route\RouteGroup $route) use ($container) {
    $route->map('GET', '/dashboard/basics-ETL', [$container->get(UserDashboardBasics::class), 'basicEtlDashboard']);
});
